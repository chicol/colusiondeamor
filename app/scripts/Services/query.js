'use strict';

/**
 * @ngdoc service
 * @name listadesperaApp.Query
 * @description
 * # Query
 * Service in the Colusiondeamor
 */
var app = angular.module('Colusiondeamor');

  app.service('Code', function ($resource, $httpParamSerializerJQLike) {

    var consulta = $resource('http://www.alafila.cl/code/Prontoctrl/:action', null,
      {
      'Coding': { method:'POST',
                  params: { action : 'generator' },
                headers : {"Content-Type": "application/x-www-form-urlencoded"},
                   transformRequest: function(data) {
                  return $httpParamSerializerJQLike(data);
                      }}
      });

      return consulta;
  });

  app.service('Query', function ($resource, $httpParamSerializerJQLike) {

    var consu = $resource('http://www.alafila.cl/igniter/HernannMovil/:action', null,
      {
           'total': { method:'POST',
                      params: { action : 'getdatoCHILE' },
                      headers : {"Content-Type": "application/x-www-form-urlencoded"},
                      transformRequest: function(data) {
                      return $httpParamSerializerJQLike(data);
                      }},
            'datamain': { method:'POST',
                      params: { action : 'getdatoCHIlEMain' },
                      headers : {"Content-Type": "application/x-www-form-urlencoded"},
                      transformRequest: function(data) {
                      return $httpParamSerializerJQLike(data);
                  }},
            'donante': { method:'POST',
                       params: { action : 'registrodonante' },
                       headers : {"Content-Type": "application/x-www-form-urlencoded"},
                       transformRequest: function(data) {
                       return $httpParamSerializerJQLike(data);
                  }}
      });

      return consu;
  });

  app.service('Save', function() {

    return {
        setData: function(nuevaData) {
          localStorage.data = JSON.stringify(nuevaData);
        },
        getData: function() {
          var data = null;
          try{
              data = JSON.parse(localStorage.data);
          } catch(e){
              data = null;
          }
          return data;
        },
        eraseData: function() {
          localStorage.data = null;
        }
    };
  });
