'use strict';

/**
 * @ngdoc overview
 * @name Colusiondeamor
 * @description
 * # Colusiondeamor
 *
 * Main module of the application.
 */
var app = angular.module('Colusiondeamor', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'LocalStorageModule',
    'ngMaterial',
    'oitozero.ngSweetAlert',
    'md.data.table',
    'matchmedia-ng',
    'angular-simple-sidebar'
  ]);



  app.config(['localStorageServiceProvider', function(localStorageServiceProvider){

    localStorageServiceProvider.setPrefix('ls');
  }]);

  app.config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/gracias', {
        templateUrl: 'views/other.html',
        controller: 'OtherCtrl',
        controllerAs: 'other'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
