'use strict';

/**
 * @ngdoc function
 * @name Colusiondeamor.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the Colusiondeamor
 */
angular.module('Colusiondeamor')

.controller('MainCtrl', function($scope, Save, $location, Query, $window,$mdDialog) {


  $scope.gofb = function(){
    $window.location.href = 'https://www.facebook.com/groups/393769814294402';
  }

  $("#getting-started")
  .countdown("2017/02/3 12:00", function(event) {
  $(this).text(
  event.strftime('%H:%M:%S')
  );
  });

  $scope.showAdvanced = function(ev) {
  $mdDialog.show({
    controller: DialogController,
    templateUrl: 'views/frecuentes.html',
    parent: angular.element(document.body),
    targetEvent: ev,
    clickOutsideToClose:true,
    fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
  })

  .then(function(answer) {
    $scope.status = 'You said the information was "' + answer + '".';
  }, function() {
    $scope.status = 'You cancelled the dialog.';
  });
};

  $scope.check = true;

  if ($scope.check) {
    var response = 1;
  }
  else {
    response = 0;
  }
  $scope.send = function(data,check) {

    if (data.nombre == null || data.rut == null || data.correo == null) {

      swal({
        title: 'Campos incorrectos!',
        text: 'Revisa que nombre y rut esten ingresados correctamente.',
        timer: 2000
      }).then(
        function() {},
        // handling the promise rejection
        function(dismiss) {
          if (dismiss === 'timer') {
            console.log('I was closed by the timer')
          }
        }
      )

    } else {



      Query.donante({
        nombre: $scope.send.nombre,
        rut: $scope.send.rut,
        correo: $scope.send.correo,
        newsletter:response

      }, function() {

        console.log('success');
        $window.location.href = 'https://vote2.evoting.cl/elections/district?config_url=https%3A%2F%2Fballot-box2.evoting.cl%2Felection%2F9HWxysAA&district=150';

      })

    }
  };

  $scope.num = parseInt(0);

  Query.datamain({


  }, function(response) {

 if (response.id===null) {

     $scope.num = 0;
     $scope.total = 0;
 }
 else {
   var num = parseInt(response.id);

   var total = num * 7000;

   $scope.num = num;
   $scope.total = total;
 }

  })

  $scope.$location = $location;

  $scope.state = false;
  $scope.menuTitle = '#Colusiondeamor';
  $scope.settings = {
    close: true,
    closeIcon: "fa fa-times"
  };

  $scope.items = [{
    name: "La idea",
    link: "#/#idea"
  }, {
    name: "En qué creemos",
    link: "#/#creer"
  }, {
    name: "Quiero colaborar",
    link: "#/#donar"
  }];

  function DialogController($scope, $mdDialog) {
  $scope.hide = function() {
    $mdDialog.hide();
  };

  $scope.cancel = function() {
    $mdDialog.cancel();
  };

  $scope.answer = function(answer) {
    $mdDialog.hide(answer);
  };
}
});
